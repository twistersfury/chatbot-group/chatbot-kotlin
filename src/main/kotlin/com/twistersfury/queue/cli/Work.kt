@file:OptIn(ExperimentalCli::class)

package com.twistersfury.queue.cli

import com.twistersfury.shared.di.interfaces.DiInterface
import com.twistersfury.shared.di.interfaces.InjectionAwareInterface
import com.twistersfury.shared.logger.interfaces.LoggerInterface

import kotlinx.cli.*

class Work(private val diInterface: DiInterface): Subcommand("work", "Run Worker Instance"), InjectionAwareInterface {
    val queueName: String by argument(ArgType.String, "queueName", "Queue To Process")
    val maxItems: Int by option(ArgType.Int, "max-items", "i", "Max Items To Process").default(0)
    val reserveTimeout: Int by option(ArgType.Int, "reserve-timeout", "r", "Max Time To Wait To Reserve Job").default(0)
    val sleepMs: Int by option(ArgType.Int, "sleep", "s", "Number of Milliseconds To Sleep").default(0)

    override fun getDi(): DiInterface
    {
        return this.diInterface
    }
    override fun execute() {
        (this.getDi().get("logger") as LoggerInterface).info("Working!")
    }
}