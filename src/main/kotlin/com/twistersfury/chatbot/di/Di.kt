package com.twistersfury.chatbot.di

import com.twistersfury.shared.di.AbstractDi
import com.twistersfury.shared.logger.interfaces.LoggerInterface
import com.twistersfury.shared.logger.StandardOut

object Di: AbstractDi() {
    init {
        this.set("logger", StandardOut::class.qualifiedName!!)
    }

    fun getLogger(): LoggerInterface {
        return this.get("logger") as LoggerInterface
    }
}