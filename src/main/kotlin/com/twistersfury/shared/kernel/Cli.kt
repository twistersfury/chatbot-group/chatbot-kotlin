@file:OptIn(ExperimentalCli::class)

package com.twistersfury.shared.kernel

import com.twistersfury.queue.cli.Work
import com.twistersfury.shared.di.interfaces.DiInterface
import kotlinx.cli.*

class Cli(private val diInterface: DiInterface) {
    @OptIn(ExperimentalCli::class)
    fun handle(args: Array<String>) {
        val parser: ArgParser = this.diInterface.get(
            ArgParser::class.qualifiedName!!, "chat-bot",
            true,
            ArgParser.OptionPrefixStyle.LINUX,
            false,
            false
        ) as ArgParser

        parser.subcommands(*this.buildCommands())
        parser.parse(args)
    }

    @OptIn(ExperimentalCli::class)
    private fun buildCommands(): Array<Subcommand> {
        return arrayOf(
            this.diInterface.get(Work::class.qualifiedName!!, this.diInterface) as Subcommand
        )
    }
}