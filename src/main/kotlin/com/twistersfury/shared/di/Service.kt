package com.twistersfury.shared.di

import com.twistersfury.shared.di.interfaces.DiInterface
import com.twistersfury.shared.di.interfaces.ServiceInterface

class Service(private val diInterface: DiInterface, private val serviceImplementation: Any): ServiceInterface {
    override fun get(vararg args: Any): Any {
        var service: Any = this.serviceImplementation

        if (this.serviceImplementation is String) {
            service = this.getInstance(this.serviceImplementation.toString(), *args)
        }

        return service
    }

    private fun getInstance(serviceImplementation: String, vararg args: Any): Any {
        return this.diInterface.get(serviceImplementation, *args)
    }
}