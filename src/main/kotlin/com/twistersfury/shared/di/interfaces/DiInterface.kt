package com.twistersfury.shared.di.interfaces

interface DiInterface {
    fun get(serviceName: String, vararg args: Any): Any

    fun set(serviceName: String, serviceImplementation: Any): DiInterface

    fun has(serviceName: String): Boolean
}