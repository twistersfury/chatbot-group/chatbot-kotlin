package com.twistersfury.shared.di.interfaces

interface ServiceInterface {
    fun get(vararg args: Any): Any
    //fun setClass(className: String): ServiceInterface
}