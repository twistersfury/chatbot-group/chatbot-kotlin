package com.twistersfury.shared.di.interfaces

interface InjectionAwareInterface {
    fun getDi(): DiInterface
}