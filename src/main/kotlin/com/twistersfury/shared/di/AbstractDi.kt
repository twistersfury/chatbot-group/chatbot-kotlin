package com.twistersfury.shared.di

import com.twistersfury.shared.di.interfaces.DiInterface
import com.twistersfury.shared.di.interfaces.ServiceInterface
import kotlin.reflect.full.createInstance
import kotlin.reflect.full.primaryConstructor

abstract class AbstractDi(private val serviceInterface: String = Service::class.qualifiedName!!): DiInterface {
    private var services: HashMap<String, ServiceInterface> = hashMapOf()

    override fun get(serviceName: String, vararg args: Any): Any {
        if (!this.has(serviceName)) {
            return this.buildInstance(serviceName, *args)
        }

        return this.getService(serviceName).get(*args)
    }

    override fun has(serviceName: String): Boolean {
        return this.services.containsKey(serviceName)
    }

    override fun set(serviceName: String, serviceImplementation: Any): AbstractDi {
        val service: ServiceInterface = if (serviceImplementation is ServiceInterface) {
            serviceImplementation
        } else if (serviceImplementation is String && serviceName == serviceImplementation.toString()) {
            throw Exception("Service Name Cannot Match Implementation Name")
        } else {
            this.buildService(serviceImplementation)
        }

        this.services.put(serviceName, service)

        return this
    }

    /**
     * Builds Service Instance
     */
    private fun buildService(serviceImplementation: Any): ServiceInterface {
        return this.buildInstance(
            this.serviceInterface,
            this,
            serviceImplementation
        ) as ServiceInterface
    }

    private fun buildInstance(serviceImplementation: String, vararg args: Any): Any {
        try {
            if (args.isEmpty()) {
                return Class.forName(serviceImplementation).kotlin.createInstance()
            }

            val constructor = Class.forName(serviceImplementation).kotlin.primaryConstructor

            if (args.size != constructor!!.parameters.size) {
                throw Exception("Invalid Constructor Args")
            }

            return constructor.call(*args) ?: throw ClassNotFoundException()
        } catch (exception: ClassNotFoundException) {
            throw Exception("Service Not Defined")
        }
    }

    private fun getService(serviceName: String): ServiceInterface {
        return this.services[serviceName]!!
    }
}