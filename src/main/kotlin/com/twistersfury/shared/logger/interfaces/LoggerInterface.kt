package com.twistersfury.shared.logger.interfaces

interface LoggerInterface {
    fun debug(message: String): LoggerInterface
    fun info(message: String): LoggerInterface
    fun notice(message: String): LoggerInterface
    fun warning(message: String): LoggerInterface
    fun alert(message: String): LoggerInterface
    fun error(message: String): LoggerInterface
}