package com.twistersfury.shared.logger

import com.twistersfury.shared.logger.interfaces.LoggerInterface

class StandardOut: LoggerInterface {
    override fun debug(message: String): LoggerInterface {
        return this.logMessage(message, 0)
    }

    override fun info(message: String): LoggerInterface {
        return this.logMessage(message, 1)
    }

    override fun notice(message: String): LoggerInterface {
        return this.logMessage(message, 2)
    }

    override fun warning(message: String): LoggerInterface {
        return this.logMessage(message, 4)
    }

    override fun alert(message: String): LoggerInterface {
        return this.logMessage(message, 8)
    }

    override fun error(message: String): LoggerInterface {
        return this.logMessage(message, 16)
    }

    private fun logMessage(message: String, level: Int): StandardOut {
        println("${level.toString()}: $message")

        return this
    }
}