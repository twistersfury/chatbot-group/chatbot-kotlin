import com.twistersfury.shared.kernel.Cli
import com.twistersfury.chatbot.di.Di
fun main(args: Array<String>) {
    val cli: Cli = Cli(Di)
    cli.handle(args)
}