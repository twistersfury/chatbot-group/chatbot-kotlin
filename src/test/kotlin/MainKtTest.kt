import com.twistersfury.shared.logger.StandardOut
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.ByteArrayOutputStream
import java.io.PrintStream

class MainKtTest {
    private val originalStream: PrintStream = System.out
    private val outStream: ByteArrayOutputStream = ByteArrayOutputStream()

    private lateinit var testSubject: StandardOut
    @BeforeEach
    fun setUp() {
        System.setOut(PrintStream(this.outStream))

        this.testSubject = StandardOut()
    }

    @AfterEach
    fun tearDown() {
        System.setOut(this.originalStream)
    }

    @Test
    fun testMain() {
        main(arrayOf("work", "irc"))
        kotlin.test.assertEquals("1: Working!", this.outStream.toString().trim())
    }
}