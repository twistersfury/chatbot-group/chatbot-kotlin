package com.twistersfury.queue.tests.cli

import com.twistersfury.queue.cli.Work
import com.twistersfury.shared.di.interfaces.DiInterface
import com.twistersfury.shared.logger.interfaces.LoggerInterface
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.BeforeEach

class WorkTest {
    private lateinit var testSubject: Work

    @BeforeEach
    fun setUp() {
        val mockLogger = mockk<LoggerInterface>()
        every { mockLogger.info("Working!") } returns mockLogger

        val diInstance = mockk<DiInterface>()
        every { diInstance.get("logger") } returns mockLogger

        this.testSubject = Work(diInstance)
    }

    @Test
    fun testExecute() {
        this.testSubject.execute()
    }
}