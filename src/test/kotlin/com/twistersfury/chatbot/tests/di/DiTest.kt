package com.twistersfury.chatbot.tests.di

import com.twistersfury.chatbot.di.Di
import com.twistersfury.shared.logger.StandardOut
import io.mockk.clearAllMocks
import io.mockk.mockk
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class DiTest {
    private lateinit var testSubject: Di

    @BeforeEach
    fun setUp() {
        this.testSubject = Di
    }

    @AfterEach
    fun tearDown() {
        clearAllMocks()
    }

    @Test
    fun testLogger() {
        val stubLogger = mockk<StandardOut>()

        this.testSubject.set(StandardOut::class.qualifiedName!!, stubLogger)

        assertSame(this.testSubject.getLogger(), stubLogger)
    }
}