package com.twistersfury.shared.tests.di

import com.twistersfury.shared.di.AbstractDi
import com.twistersfury.shared.di.Service
import io.mockk.every
import io.mockk.mockk
import kotlinx.cli.ArgParser
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.Assertions.*
import org.junit.jupiter.api.BeforeEach

class AbstractDiTest {
    private lateinit var testSubject: AbstractDi

    @BeforeEach
    fun setUp() {
        this.testSubject = EmptyDi()
    }

    @Test
    fun testClassInitialization() {
        val mockService = mockk<Service>()

        every { mockService.get(*anyVararg() ) } returns this

        this.testSubject.set(Service::class.toString(), mockService)

        assertTrue(this.testSubject.get(AbstractDiTest::class.qualifiedName!!) is AbstractDiTest)
    }

    @Test
    fun testServiceNotDefined() {
        val exception = assertThrows(Exception::class.java) {
            this.testSubject.get("Something That Doesn't Exist")
        }

        assertEquals("Service Not Defined", exception.message)
    }

    @Test
    fun testService() {
        val mockService = mockk<Service>()

        every { mockService.get(*anyVararg() ) } returns this

        this.testSubject.set(Service::class.qualifiedName!!, mockService)
        assertSame(this, this.testSubject.get(Service::class.qualifiedName!!))
    }

    @Test
    fun testSet() {
        this.testSubject.set("something", AbstractDiTest::class.qualifiedName!!)
        assertInstanceOf(AbstractDiTest::class.java, this.testSubject.get("something"))
    }

    @Test
    fun testServiceNameMatchesInvocation() {
        val exception = assertThrows(Exception::class.java) {
            this.testSubject.set("something", "something")
        }

        assertEquals("Service Name Cannot Match Implementation Name", exception.message)
    }

    @Test
    fun testServiceArgsException() {
        val exception = assertThrows(Exception::class.java) {
            this.testSubject.get(ArgParser::class.qualifiedName!!, "something")
        }

        assertEquals("Invalid Constructor Args", exception.message)
    }
}