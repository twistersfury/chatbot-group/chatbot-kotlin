package com.twistersfury.shared.tests.di

import com.twistersfury.shared.di.interfaces.DiInterface
import com.twistersfury.shared.di.Service
import io.mockk.every
import io.mockk.mockk
import org.junit.jupiter.api.Test

import org.junit.jupiter.api.Assertions.*

class ServiceTest {
    private lateinit var testSubject: Service

    @Test
    fun getWithParameters() {
        val diInstance = mockk<DiInterface>()

        every { diInstance.get(ServiceTest::class.qualifiedName!!) } returns this

        this.testSubject = Service(diInstance, ServiceTest::class.qualifiedName!!)

        assertSame(this.testSubject.get(), this)
    }

    @Test
    fun getWithoutParameters() {
        val diInstance = mockk<DiInterface>()

        every { diInstance.get(ServiceTest::class.qualifiedName!!) } returns Service(diInstance, ServiceTest::class.qualifiedName!!)

        this.testSubject = Service(diInstance, ServiceTest::class.qualifiedName!!)

        assertTrue(this.testSubject.get() is Service)
    }
}