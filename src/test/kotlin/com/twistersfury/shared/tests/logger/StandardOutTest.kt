package com.twistersfury.shared.tests.logger

import com.twistersfury.shared.logger.StandardOut
import org.junit.jupiter.api.AfterEach
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import java.io.ByteArrayOutputStream
import java.io.PrintStream
import kotlin.test.assertEquals

class StandardOutTest {
    private val originalStream: PrintStream = System.out
    private val outStream: ByteArrayOutputStream = ByteArrayOutputStream()

    private lateinit var testSubject: StandardOut
    @BeforeEach
    fun setUp() {
        System.setOut(PrintStream(this.outStream))

        this.testSubject = StandardOut()
    }

    @AfterEach
    fun tearDown() {
        System.setOut(this.originalStream)
    }

    @Test
    fun debug() {
        this.testSubject.debug("Hello World!")
        assertEquals("0: Hello World!", this.outStream.toString().trim())
    }

    @Test
    fun info() {
        this.testSubject.info("Hello World!")
        assertEquals("1: Hello World!", this.outStream.toString().trim())
    }

    @Test
    fun notice() {
        this.testSubject.notice("Hello World!")
        assertEquals("2: Hello World!", this.outStream.toString().trim())
    }

    @Test
    fun warning() {
        this.testSubject.warning("Hello World!")
        assertEquals("4: Hello World!", this.outStream.toString().trim())
    }

    @Test
    fun alert() {
        this.testSubject.alert("Hello World!")
        assertEquals("8: Hello World!", this.outStream.toString().trim())
    }

    @Test
    fun error() {
        this.testSubject.error("Hello World!")
        assertEquals("16: Hello World!", this.outStream.toString().trim())
    }
}