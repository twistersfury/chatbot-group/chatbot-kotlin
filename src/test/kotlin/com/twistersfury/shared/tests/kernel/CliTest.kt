package com.twistersfury.shared.tests.kernel

import com.twistersfury.queue.cli.Work
import com.twistersfury.shared.di.interfaces.DiInterface
import com.twistersfury.shared.kernel.Cli
import io.mockk.every
import io.mockk.mockk
import kotlinx.cli.ArgParser
import kotlinx.cli.ArgParserResult
import kotlinx.cli.ExperimentalCli
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test

class CliTest {
    private lateinit var testSubject: Cli

    @OptIn(ExperimentalCli::class)
    @BeforeEach
    fun setUp() {
        val stubResults = mockk<ArgParserResult>()
        val stubWork = mockk<Work>()

        val mockParser = mockk<ArgParser>()

        every { mockParser.subcommands(*anyVararg()) } returns Unit
        every { mockParser.parse(arrayOf("something")) } returns stubResults

        val mockDi = mockk<DiInterface>()
        every { mockDi.get(ArgParser::class.qualifiedName!!, *anyVararg()) } returns mockParser
        every { mockDi.get(Work::class.qualifiedName!!, mockDi) } returns stubWork

        this.testSubject = Cli(mockDi)
    }

    @Test
    fun testHandle() {
        this.testSubject.handle(arrayOf("something"))
    }
}